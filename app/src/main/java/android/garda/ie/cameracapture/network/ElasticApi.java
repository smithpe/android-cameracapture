package android.garda.ie.cameracapture.network;



import android.garda.ie.cameracapture.model.ElasticRequest;
import android.garda.ie.cameracapture.model.ElasticResponse;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ElasticApi {
    @POST("image-lectures/_doc/")
    Single<ElasticResponse> saveLocation(@Header("Authorization") String token, @Body ElasticRequest request);

}
