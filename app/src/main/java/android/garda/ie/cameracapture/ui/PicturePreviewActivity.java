package android.garda.ie.cameracapture.ui;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.garda.ie.cameracapture.R;
import android.garda.ie.cameracapture.model.ElasticRequest;
import android.garda.ie.cameracapture.model.Image;
import android.garda.ie.cameracapture.network.ElasticApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.material.button.MaterialButton;
import com.otaliastudios.cameraview.BitmapCallback;
import com.otaliastudios.cameraview.PictureResult;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class PicturePreviewActivity extends Activity implements LocationListener, SensorEventListener {

    public SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
    private static PictureResult picture;
    protected Location location;
    float direction;
    SensorManager sensorManager;
    ElasticApi api;
    String bitmapBase64;
    String deviceId;
    String projectId;
    float azimut;
    float[] mGravity;
    float[] mGeomagnetic;
    Button buttonUpload;
    EditText commentText;
    byte[] imageData;

    public static void setPictureResult(@Nullable PictureResult pictureResult) {
        picture = pictureResult;
    }

    @SuppressWarnings("MissingPermission")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        location = getLocation();
        setContentView(R.layout.activity_picture_preview);
        buttonUpload = (Button)findViewById(R.id.btn_upload);
        RadioGroup radioGroup = (RadioGroup)findViewById(R.id.radioGroup1);
        commentText = ((EditText)findViewById(R.id.comment));
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            projectId = ((RadioButton)findViewById(group.getCheckedRadioButtonId())).getText().toString();
            buttonUpload.setEnabled(true);

        });

        buttonUpload.setOnClickListener(view -> {
            saveImage();
        });

        //Add permission check
        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        final String tmDevice, tmSerial, androidId;

            tmDevice = "" + tm.getDeviceId();
            tmSerial = "" + tm.getSimSerialNumber();
            androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        deviceId = deviceUuid.toString();

        api = new Retrofit.Builder()
                //user = elastic
                //pass = ldTexf2e7PfyuZtOjRhYq4nY
                .baseUrl("https://36a5bc6ce6f24bdbb7e20530a877a696.eu-west-1.aws.found.io:9243/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder()
                        .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                        .addInterceptor(chain -> {
                            Request originalRequest = chain.request();

                            Request.Builder builder = originalRequest.newBuilder().header("Authorization",
                                    Credentials.basic("elastic", "ldTexf2e7PfyuZtOjRhYq4nY"));

                            Request newRequest = builder.build();
                            return chain.proceed(newRequest);
                        })
                        .build())
                .build()
                .create(ElasticApi.class);
        final PictureResult result = picture;
        if (result == null) {
            finish();
            return;
        }

        final ImageView imageView = findViewById(R.id.image);
        final long delay = getIntent().getLongExtra("delay", 0);
        PicturePreviewActivity.this.bitmapBase64 = Base64.encodeToString(result.getData(), Base64.DEFAULT);
        result.toBitmap(1000, 1000, new BitmapCallback() {
            @Override
            public void onBitmapReady(Bitmap bitmap) {
                imageView.setImageBitmap(bitmap);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                Single.fromCallable(() -> bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                success -> imageData = stream.toByteArray(),
                                error -> Log.e("Error compressing image", error.toString()));
            }
        });

        if (result.isSnapshot()) {
            // Log the real size for debugging reason.
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(result.getData(), 0, result.getData().length, options);
            if (result.getRotation() % 180 != 0) {
                Log.e("PicturePreview", "The picture full size is " + result.getSize().getHeight() + "x" + result.getSize().getWidth());
            } else {
                Log.e("PicturePreview", "The picture full size is " + result.getSize().getWidth() + "x" + result.getSize().getHeight());
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    private Location getLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //Add permission check
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
//            public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                                                      int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            new AlertDialog.Builder(this).setMessage("Allow location permission for this app in settings").create().show();
            return null;
        } else {
            //locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, this, null);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 5, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, this);
            return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        }
    }

    @SuppressWarnings("MissingPermission")
    private void saveImage() {
        Log.i("Location", ""+location.getLatitude() + ",  " + location.getLongitude());
        android.garda.ie.cameracapture.model.Location location2 = new android.garda.ie.cameracapture.model.Location();
        location2.lat = location.getLatitude();
        location2.lon = location.getLongitude();
        ElasticRequest request = new ElasticRequest();
        request.location = location2;
        request.timeStamp = format.format(new Date());
        request.direction = direction;
        request.bitmap = bitmapBase64;
        request.mobileId = deviceId;
        request.projectId = projectId;
        request.comment = commentText.getText().toString();

        updateElastic(request);

        updateRealm(location.getLatitude(), location.getLongitude());
    }

    private void updateRealm(double lat, double lon) {
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransactionAsync(trans -> {
            try {
                Image image = new Image();
                image.setId(UUID.randomUUID().toString());
                image.setData(imageData);
                image.lat = lat;
                image.lon = lon;
                image.angle = direction;
                image.setComment(commentText.getText().toString());
                image.setProjectId(projectId);
                image.setTimestamp(new Date());
                trans.insert(image);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void updateElastic(ElasticRequest request) {
        String token = "Basic ZWxhc3RpYzpsZFRleGYyZTdQZnl1WnRPalJoWXE0blk=";
        api.saveLocation(token, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(success -> new AlertDialog.Builder(this)
                        .setMessage("The record was uploaded successfully for Location" + location.getLatitude() + ",  " + location.getLongitude())
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                finish();
                            }
                        })
                        .create()
                        .show(), error -> {
                    Log.e("Elastic Error", "Error calling elastic API");
                    new AlertDialog.Builder(this)
                            .setMessage("Upload error!")
                            .setPositiveButton("OK", (dialogInterface, i) -> dialogInterface.dismiss())
                            .create()
                            .show();
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!isChangingConfigurations()) {
            setPictureResult(null);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("onLocationChanged()", ""+location.getLatitude() + ",  " + location.getLongitude());
        this.location = location;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(direction == 0) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
                mGravity = event.values;
            if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
                mGeomagnetic = event.values;
            if (mGravity != null && mGeomagnetic != null) {
                float R[] = new float[9];
                float outR[] = new float[9];
                float I[] = new float[9];

                boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
                if (success) {
                    float orientation[] = new float[3];

                    SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_X, SensorManager.AXIS_Y, outR);

                    SensorManager.getOrientation(outR, orientation);
                    azimut = orientation[0];

                    float degree = (float) (Math.toDegrees(azimut) + 360) % 360;

                    System.out.println("degree " + degree);
                    direction = degree;
                }
            }
        }
        else
            sensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

}
