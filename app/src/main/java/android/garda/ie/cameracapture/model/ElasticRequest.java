package android.garda.ie.cameracapture.model;

import com.google.gson.annotations.SerializedName;

public class ElasticRequest {

    @SerializedName("timestamp")
    public String timeStamp;

    @SerializedName("location")
    public Location location;

    @SerializedName("direction")
    public float direction;

    @SerializedName("bitmap")
    public String bitmap;

    @SerializedName("mobile_id")
    public String mobileId;

    @SerializedName("project_id")
    public String projectId;

    @SerializedName("comment")
    public String comment;
}
