package android.garda.ie.cameracapture;

import android.app.Application;
import android.util.Log;

import io.realm.ObjectServerError;
import io.realm.Realm;
import io.realm.SyncConfiguration;
import io.realm.SyncCredentials;
import io.realm.SyncUser;

import static android.garda.ie.cameracapture.Constants.AUTH_URL;

public class CameraApplication extends Application {

    @Override
    public void onCreate(){
        super.onCreate();
        Realm.init(this);
        SyncCredentials credentials = SyncCredentials.usernamePassword(Constants.REALM_USER, Constants.REALM_PASSWORD);
        SyncUser.logInAsync(credentials, AUTH_URL, new SyncUser.Callback<SyncUser>() {
            @Override
            public void onSuccess(SyncUser user) {
                try {
                    SyncConfiguration configuration = SyncUser.current().getDefaultConfiguration();
                    Realm.setDefaultConfiguration(configuration);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ObjectServerError error) {
                Log.e("Login error", error.toString());
            }
        });
    }

}
